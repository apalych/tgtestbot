package main

import (
	"fmt"
	"os"
)

func AppendFile(str string) error {
	file, err := os.OpenFile(serialsFileName, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil  {
		return fmt.Errorf("error opening file")
	}
	defer file.Close()

	_, err = file.WriteString(str)

	if err != nil {
		return fmt.Errorf("error writing to file")
	}

	return nil
}