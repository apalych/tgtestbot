package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/go-telegram-bot-api/telegram-bot-api"
)

const (
	downloadExcel = "Скачать в excel"
	closeMenu = "Закрыть меню"
	botToken = ""
	excelFileName = "Book1.xlsx"
	serialsFileName = "serials.txt"
)

func main() {
	bot, err := tgbotapi.NewBotAPI(botToken)
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil { // ignore any non-Message Updates
			continue
		}

		log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)

		switch update.Message.Text {
		case "menu":
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, update.Message.Text)
			msg.ReplyMarkup = menu()
			bot.Send(msg)

			continue
		case "close", closeMenu:
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, update.Message.Text)
			msg.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)
			bot.Send(msg)

			continue
		case downloadExcel:
			// создаём файл xlsx
			err := CreateXlsxFromFile()
			if err != nil {
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Ошибка создания xls-файла")
				bot.Send(msg)
				continue
			}

			// удаляем файл с серийниками
			err = os.Remove(serialsFileName)
			if err != nil {
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Ошибка очистки старого файла с серийниками")
				bot.Send(msg)
				continue
			}

			// отправляем файл
			msg := tgbotapi.NewDocumentUpload(update.Message.Chat.ID, excelFileName)
			bot.Send(msg)

			// удаляем эксель файл
			err = os.Remove(excelFileName)
			if err != nil {
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Ошибка очистки xlsx-файла")
				bot.Send(msg)
				continue
			}
			continue
		default:
			str, err := findSerialFromMessage(update.Message.Text)
			if err != nil {
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Ошибка распознавания")
				bot.Send(msg)
				continue
			}

			err = AppendFile(fmt.Sprintf("%s\n", str))
			if err != nil {
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, err.Error())
				bot.Send(msg)
				continue
			}
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, fmt.Sprintf("В файл добавлено значение %s", str))

			bot.Send(msg)

			continue
		}
	}
}

func findSerialFromMessage(mes string) (string, error) {
	strSlice := strings.Split(mes, "Текст:")
	if len(strSlice) < 2 {
		return "", fmt.Errorf("error in input")
	}

	strSlice1 := strings.Split(strSlice[1], "\n")
	if len(strSlice) < 1 {
		return "", fmt.Errorf("error in input")
	}
	return strings.TrimSpace(strSlice1[0]), nil
}

func menu() tgbotapi.ReplyKeyboardMarkup {
	keyboard := tgbotapi.NewReplyKeyboard(
		tgbotapi.NewKeyboardButtonRow(
			tgbotapi.NewKeyboardButton(downloadExcel),
			tgbotapi.NewKeyboardButton(closeMenu),
		))
	return keyboard
}