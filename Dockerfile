FROM golang:1.17.1 AS builder
WORKDIR /app
COPY . .
RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

FROM alpine:latest
WORKDIR /src/
COPY --from=builder /app .

ENTRYPOINT ["./app"]
