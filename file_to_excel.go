package main

import (
	"bufio"
	"fmt"
	"github.com/360EntSecGroup-Skylar/excelize"
	"os"
	"strconv"
)

const (
	defaultSheet = "Sheet1"
)

func CreateXlsxFromFile() error {
	f := excelize.NewFile()

	file, err := os.OpenFile(serialsFileName, os.O_RDONLY, 0666)
	if err != nil  {
		return fmt.Errorf("error opening file")
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	cellNum := 0
	j := 0
	for scanner.Scan() {
		j++
		str := scanner.Text()
		cellNumStr := strconv.Itoa(cellNum + j)
		f.SetCellValue(defaultSheet, "A"+cellNumStr, str)
	}

	// Save spreadsheet by the given path.
	if err := f.SaveAs(excelFileName); err != nil {
		return err
	}

	return nil
}