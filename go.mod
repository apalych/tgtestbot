module tgBotTest

go 1.17

require (
	github.com/360EntSecGroup-Skylar/excelize v1.4.1 // indirect
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible // indirect
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826 // indirect
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
)
